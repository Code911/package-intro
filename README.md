# Intro addon for codebox

This addon gives brief introduction to Codebox IDE. Internally it uses
[hopscotch](http://linkedin.github.io/hopscotch/) library to display
introductory bubbles. The plugin is adjusted to Codebox environment.

## Modifications

In file src/index.js, there is a tour object defined. Add step that will be
displayed on page during startup. Example:

    {
      target: document.querySelector('.tabs-section-header .active .octicon-globe'),
      title: "Board view",
      content: "This tab displays board view",
      placement: "bottom",
      showPrevButton: true
    }

For more information, check [hopscotch](http://linkedin.github.io/hopscotch/) documentation.


## Installation

This addon requires webview addon to be installed as well, because it
introduces to IoTIFY-specific features added to Codebox.

To install this addon in your codebox instance, run command:

    ./bin/codebox.js install -p intro:git@bitbucket.org:Code911/package-intro.git

## Settings

This addon has single setting, that will by default display introduction on
startup. When introduction tour is finished by user, setting will be turned
off. It can be modified anytime in Codebox Settings.
