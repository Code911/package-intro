require("./stylesheets/style.less");
var _ = codebox.require("hr.utils");
var dialogs = codebox.require("utils/dialogs");
var commands = codebox.require("core/commands");
var FirstTimeView = require("./views/first-time");
var hopscotch = require("./hopscotch");
var bubble = require("./templates/bubble.html");
var settings = require("./settings");

codebox.app.once("ready", function() {
  var settings = codebox.settings.exportJson();
  commands.register({
    id: "tour.start",
    title: "Start tour",
    run: function(args, context) {
      var tour = {
        id: "codebox-tour",
        steps: [
        {
          target: document.querySelector(".component-panel-toolbar"),
          title: "Terminal",
          content: 'Click <span class="octicon octicon-terminal"></span> to open a SSH session to Simulator. Simulator could take a while to boot so please retry later in case of failure.',
          placement: "right",
        },
        {
          target: document.querySelector(".component-panel-toolbar"),
          title: "Board view",
          content: 'Click <span class="octicon octicon-globe"></span> to open project view. Here you could interact with virtual sensors and hardware.',
          placement: "right",
          showPrevButton: true,
        },
        {
          target: document.querySelector('.tabs-section-header .active .octicon-globe'),
          title: "Board view",
          content: "This tab displays view of your project. You could alway close and opne another one.",
          placement: "bottom",
          showPrevButton: true
        }
        ],
        onEnd: function() {
          var s = codebox.settings.exportJson();
          console.log("onEnd");
          if (s.intro) {
            s.intro.show = false;
            codebox.settings.importJSON(s);
          }
        }
      };

      hopscotch.setRenderer(function(opts) {
        return _.template(bubble)(opts);
      });
      
      hopscotch.startTour(tour);
    }
  });

  if (settings.intro && settings.intro.show) {
    dialogs.open(FirstTimeView, {
      size: "medium"
    });
  }
});

