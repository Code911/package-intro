var View = codebox.require("hr.view");
var template = require("../templates/dialog.html");
var commands = codebox.require("core/commands");

var Dialog = View.Template.extend({
    tagName: "div",
    className: "component-first-time",
    template: template,
    events: {
        "click .do-confirm": "doConfirm",
        "click .do-cancel": "doCancel",
    },

    initialize: function() {
        Dialog.__super__.initialize.apply(this, arguments);
    },

    render: function() {
        return Dialog.__super__.render.apply(this, arguments);
    },

    finish: function() {
        return Dialog.__super__.finish.apply(this, arguments);
    },

    // Confirm and start intro tour
    doConfirm: function(e) {
        if (e) e.preventDefault();
        commands.run("tour.start");
        this.parent.close(e);
    },

    // Cancel tour and don't ask again
    doCancel: function(e) {
        if (e) e.preventDefault();
        var s = codebox.settings.exportJson();
        if (s.intro) {
            s.intro.show = false;
            codebox.settings.importJSON(s);
        }
        this.parent.close(e);
    },
});

module.exports = Dialog;
