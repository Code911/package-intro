module.exports = codebox.settings.schema("intro",
    {
        "title": "Intro",
        "type": "object",
        "properties": {
            "show": {
                "description": "Show Intro on startup",
                "type": "boolean",
                "default": true
            }
        }
    }
);
